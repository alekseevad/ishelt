FROM gradle:latest AS BUILD

VOLUME /tmp

WORKDIR /app
COPY . /app

RUN gradle build --exclude-task test

FROM openjdk:17-jdk-alpine

WORKDIR /app
COPY --from=BUILD /app .
EXPOSE 8080
ENTRYPOINT exec java -jar /app/build/libs/i-shelt-0.0.1-SNAPSHOT.jar