FROM minio/mc

ENTRYPOINT ["/bin/sh", "-c", "until (/usr/bin/mc config host add myminio http://minio:9000 minio minio123) do echo '...waiting...' && sleep 1; done && /usr/bin/mc mb myminio/test && /usr/bin/mc policy set download myminio/test && exit 0"]