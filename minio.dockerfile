FROM quay.io/minio/minio

CMD ["server", "/data", "--console-address", ":9090", "--address", ":9000"]

VOLUME ["/data", "/root/.minio"]

EXPOSE 9000