FROM postman/newman_alpine33

WORKDIR /collections
ARG COLLECTION
COPY ./collections/* /collections/

ENTRYPOINT ["newman", "run", "/collections/all_actions.postman_collection.json", "-g", "/collections/global_vars.postman_environment.json"]