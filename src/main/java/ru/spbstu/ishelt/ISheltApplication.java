package ru.spbstu.ishelt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ISheltApplication {

	public static void main(String[] args) {
		SpringApplication.run(ISheltApplication.class, args);
	}

}
