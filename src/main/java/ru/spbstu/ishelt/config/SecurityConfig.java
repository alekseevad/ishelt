package ru.spbstu.ishelt.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.spbstu.ishelt.security.JwtTokenFilter;

@AllArgsConstructor
@Configuration
@EnableWebSecurity
public class SecurityConfig {
    private final JwtTokenFilter jwtTokenFilter;
    private final AuthenticationProvider authProvider;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf()
                    .disable()
                .authorizeHttpRequests()
                    .requestMatchers( "/api/v1/auth/**").permitAll()
                    .requestMatchers("/api/v1/users/create_user").permitAll()
                    .requestMatchers("/api/v1/posts/get_post/{id}").permitAll()
                    .requestMatchers("/api/v1/posts/get_posts/all").permitAll()
                    .requestMatchers("/api/v1/animals/get_animal/{id}").permitAll()
                    .requestMatchers("/api/v1/animals/get_animals/all").permitAll()
                    .requestMatchers("/api/v1/lost_animals/get_lost_animal/{id}").permitAll()
                    .requestMatchers("/api/v1/lost_animals/get_lost_animals/all").permitAll()
                    .requestMatchers("/api/v1/lost_animals/get_placemark/**").permitAll()
                    .requestMatchers("/api/v1/shelters/get_placemark/**").permitAll()
                    .requestMatchers("/api/v1/shelters/get_shelter/{name}").permitAll()
                    .requestMatchers("/api/v1/shelters/get_shelters/all").permitAll()
                    .requestMatchers("/api/v1/users/profile").hasAnyAuthority("ADMIN","USER","SHELTER_OWNER", "VOLUNTEER")
                    .requestMatchers("/api/v1/users/profile/current").hasAnyAuthority("ADMIN","USER","SHELTER_OWNER", "VOLUNTEER")
                    .requestMatchers("/api/v1/users/profile/posts").hasAnyAuthority("ADMIN","USER","SHELTER_OWNER", "VOLUNTEER")
                    .requestMatchers("/api/v1/users/set_like").hasAnyAuthority("ADMIN","USER","SHELTER_OWNER", "VOLUNTEER")
                    .requestMatchers("/api/v1/users/unlike").hasAnyAuthority("ADMIN","USER","SHELTER_OWNER", "VOLUNTEER")
                    .requestMatchers("/api/v1/users/edit_user").hasAnyAuthority("ADMIN","USER","SHELTER_OWNER", "VOLUNTEER")
                    .requestMatchers("/api/v1/animals/**").hasAnyAuthority("ADMIN","USER","SHELTER_OWNER", "VOLUNTEER")
                    .requestMatchers("/api/v1/shelters/create_shelter").hasAuthority("ADMIN")
                    .requestMatchers("/api/v1/shelters/edit").hasAuthority("SHELTER_OWNER")
                    .requestMatchers("/api/v1/shelters/add_animal").hasAuthority("SHELTER_OWNER")
                    .requestMatchers("/api/v1/shelters/remove_animal").hasAuthority("SHELTER_OWNER")
                    .requestMatchers("/api/v1/shelters/add_user").hasAuthority("ADMIN")
                    .requestMatchers("/api/v1/lost_animals/**").hasAnyAuthority("ADMIN","USER","SHELTER_OWNER", "VOLUNTEER")
                    .requestMatchers("/api/v1/lost_animals/bound_animals").hasAuthority("VOLUNTEER")
                    .requestMatchers("/api/v1/posts/**").hasAnyAuthority("ADMIN","USER","SHELTER_OWNER", "VOLUNTEER")
                    .requestMatchers("/api/v1/admin/**").hasAuthority("ADMIN")
                    .requestMatchers("/api/v1/tasks/create_task").hasAuthority("SHELTER_OWNER")
                    .requestMatchers("/api/v1/tasks/get_task/{id}").hasAnyAuthority("ADMIN","USER","SHELTER_OWNER", "VOLUNTEER")
                    .requestMatchers("/api/v1/tasks/get_active_tasks/all").hasAnyAuthority("ADMIN","USER","SHELTER_OWNER", "VOLUNTEER")
                    .requestMatchers("/api/v1/tasks/get_finished_tasks/all").hasAnyAuthority("ADMIN","USER","SHELTER_OWNER", "VOLUNTEER")
                    .requestMatchers("/api/v1/tasks/respond").hasAnyAuthority("USER", "VOLUNTEER", "SHELTER_OWNER")
                    .requestMatchers("/api/v1/tasks/finish").hasAuthority("SHELTER_OWNER")
                    .requestMatchers("/api/v1/files/**").hasAnyAuthority("ADMIN","USER","SHELTER_OWNER", "VOLUNTEER")
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authenticationProvider(authProvider)
                .addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class);
        
        return http.build();
    }

}
