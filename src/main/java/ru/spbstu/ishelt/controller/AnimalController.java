package ru.spbstu.ishelt.controller;

import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.spbstu.ishelt.dto.AnimalDto;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.service.AnimalService;

@RestController
@RequestMapping("/api/v1/animals")
public class AnimalController {
    private final AnimalService animalService;

    public AnimalController(AnimalService animalService) {
        this.animalService = animalService;
    }

    @PostMapping("/create_animal")
    public ResponseEntity<GlobalResponse> createAnimal(@Valid @RequestBody AnimalDto request) throws TechException {
        return ResponseEntity.ok(animalService.createAnimal(request));
    }

    @GetMapping("/get_animal/{id}")
    public ResponseEntity<AnimalDto> getAnimal(@PathVariable("id") Long id) throws TechException {
        return ResponseEntity.ok(animalService.getAnimal(id));

    }

    @GetMapping("/get_animals/all")
    public ResponseEntity<Page<AnimalDto>> getAnimals(
            @RequestParam(value = "offset", defaultValue = "0") Integer offset,
            @RequestParam(value = "limit", defaultValue = "10") Integer limit) throws TechException {
        return ResponseEntity.ok(animalService.getAnimals(offset, limit));
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<GlobalResponse> editAnimal(@PathVariable Long id,
                                     @Valid @RequestBody AnimalDto request) throws TechException {
        return ResponseEntity.ok(animalService.editAnimal(id, request));
    }

}
