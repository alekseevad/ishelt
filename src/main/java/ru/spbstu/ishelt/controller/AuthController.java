package ru.spbstu.ishelt.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.spbstu.ishelt.dto.AuthRequestDto;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.service.AuthService;

@RestController
@RequestMapping("api/v1/auth")
public class AuthController {
    public final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/enter")
    public ResponseEntity<GlobalResponse> enter(
            @RequestBody AuthRequestDto request
    ) throws TechException {
        return ResponseEntity.ok(authService.login(request));
    }
}
