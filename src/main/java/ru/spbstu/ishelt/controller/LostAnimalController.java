package ru.spbstu.ishelt.controller;

import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.dto.LostAnimalDto;
import ru.spbstu.ishelt.dto.PlacemarkDto;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.service.LostAnimalService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/lost_animals")
public class LostAnimalController {
    private final LostAnimalService lostAnimalService;

    public LostAnimalController(LostAnimalService lostAnimalService) {
        this.lostAnimalService = lostAnimalService;
    }

    @PostMapping("/create_lost_animal")
    public ResponseEntity<GlobalResponse> createLostAnimal(@Valid @RequestBody LostAnimalDto request) throws TechException {
        return ResponseEntity.ok(lostAnimalService.createLostAnimal(request));
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<GlobalResponse> editLostAnimal(@PathVariable Long id, @Valid @RequestBody LostAnimalDto request) throws TechException {
        return ResponseEntity.ok(lostAnimalService.editLostAnimal(id, request));
    }

    @PutMapping("/bound_animals")
    public ResponseEntity<GlobalResponse> boundAnimals(@RequestParam("lost_animal_id") Long lostAnimalId, @RequestParam(name = "animal_id") Long animal_id) throws TechException {
        return ResponseEntity.ok(lostAnimalService.boundWithAnimal(lostAnimalId, animal_id));
    }

    @GetMapping("/get_lost_animal/{id}")
    public ResponseEntity<LostAnimalDto> getLostAnimal(@PathVariable Long id) throws TechException {
        return ResponseEntity.ok(lostAnimalService.getLostAnimal(id));
    }

    @GetMapping("/get_lost_animals/all")
    public ResponseEntity<Page<LostAnimalDto>> getLostAnimals(
            @RequestParam(value = "offset", defaultValue = "0") Integer offset,
            @RequestParam(value = "limit", defaultValue = "10") Integer limit) throws TechException {
        return ResponseEntity.ok(lostAnimalService.getLostAnimals(offset, limit));
    }

    @GetMapping("/get_placemark/{id}")
    public ResponseEntity<PlacemarkDto> getPlacemark(@PathVariable Long id) throws TechException {
        return ResponseEntity.ok(lostAnimalService.getPlacemark(id));
    }

    @GetMapping("/get_placemark/")
    public ResponseEntity<List<PlacemarkDto>> getAllPlacemarks() throws TechException {
        return ResponseEntity.ok(lostAnimalService.getAllPlacemarks());
    }

}
