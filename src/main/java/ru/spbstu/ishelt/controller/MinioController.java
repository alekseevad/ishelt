package ru.spbstu.ishelt.controller;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.service.MinioService;

@RestController
@RequestMapping("api/v1/files")
public class MinioController {

    private final MinioService minioService;

    public MinioController(MinioService minioService) {
        this.minioService = minioService;
    }

    @PostMapping("/upload")
    public ResponseEntity<GlobalResponse> uploadFile(
            @RequestParam(value = "file") MultipartFile file) throws TechException {
        return ResponseEntity.ok(minioService.uploadFile(file));
    }

    @GetMapping("/{filename}")
    public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable("filename") String fileName) throws TechException {
        byte[] data = minioService.downloadFile(fileName);
        ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity
                .ok()
                .contentLength(data.length)
                .header("Content-type", "application/octet-stream")
                .header("Content-disposition", "attachment; filename=\"" + fileName + "\"")
                .body(resource);
    }

}
