package ru.spbstu.ishelt.controller;

import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.dto.PostDto;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.service.PostService;

@RestController
@RequestMapping("api/v1/posts")
public class PostController {
    private final PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping("/create_post")
    public ResponseEntity<GlobalResponse> createPost(@Valid @RequestBody PostDto request) throws TechException {
        return ResponseEntity.ok(postService.createPost(request));
    }

    @GetMapping("/get_post/{id}")
    public ResponseEntity<PostDto> getPost(@PathVariable("id") Long postId) throws TechException {
        return ResponseEntity.ok(postService.getPost(postId));
    }

    @GetMapping("/get_posts/all")
    public ResponseEntity<Page<PostDto>> getPosts(
            @RequestParam(value = "offset", defaultValue = "0") Integer offset,
            @RequestParam(value = "limit", defaultValue = "10") Integer limit) throws TechException {
        return ResponseEntity.ok(postService.getPosts(offset, limit));
    }

    @PutMapping("/edit")
    public ResponseEntity<GlobalResponse> editPost(@Valid @RequestBody PostDto request) throws TechException {
        return ResponseEntity.ok(postService.editPost(request));
    }

    @PutMapping("/delete/{id}")
    public ResponseEntity<GlobalResponse> deletePost(@PathVariable("id") Long postId) throws TechException {
        return ResponseEntity.ok(postService.deletePost(postId));
    }


}
