package ru.spbstu.ishelt.controller;

import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.spbstu.ishelt.dto.CreateShelterRequestDto;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.dto.PlacemarkDto;
import ru.spbstu.ishelt.dto.ShelterDto;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.service.ShelterService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("api/v1/shelters")
public class ShelterController {
    private final ShelterService shelterService;

    public ShelterController(ShelterService shelterService) {
        this.shelterService = shelterService;
    }

    @PostMapping("/create_shelter")
    public ResponseEntity<GlobalResponse> createShelter(@Valid @RequestBody CreateShelterRequestDto request) throws TechException {
        return ResponseEntity.ok(shelterService.createShelter(request));
    }

    @GetMapping("/get_shelter/{name}")
    public ResponseEntity<ShelterDto> getShelter(@Valid @PathVariable String name) throws TechException {
        return ResponseEntity.ok(shelterService.getShelterByName(name));
    }

    @GetMapping("/get_shelters/all")
    public ResponseEntity<Page<ShelterDto>> getShelters(
            @RequestParam(value = "offset", defaultValue = "0") Integer offset,
            @RequestParam(value = "limit", defaultValue = "10") Integer limit) throws TechException {
        return ResponseEntity.ok(shelterService.getShelters(offset, limit));
    }

    @PutMapping("/edit")
    public ResponseEntity<GlobalResponse> editShelter(@RequestParam("shelter_name") String shelterName, @Valid @RequestBody CreateShelterRequestDto request) throws TechException {
        return ResponseEntity.ok(shelterService.editShelter(shelterName, request));
    }

    @PutMapping("/add_user")
    public ResponseEntity<GlobalResponse> addUser(@RequestParam("shelter_name") String shelterName, @RequestParam("username") String username) throws TechException {
        return ResponseEntity.ok(shelterService.addUser(shelterName, username));
    }

    @PutMapping("/add_animal")
    public ResponseEntity<GlobalResponse> addAnimal(@RequestParam("shelter_name") String shelterName, @RequestParam("animal_id") Long animalId, Principal principal) throws TechException {
        return ResponseEntity.ok(shelterService.addAnimal(shelterName, animalId, principal));
    }

    @PutMapping("/remove_animal")
    public ResponseEntity<GlobalResponse> removeAnimal(@RequestParam("animal_id") Long animalId) throws TechException {
        return ResponseEntity.ok(shelterService.removeAnimal(animalId));
    }

    @GetMapping("/get_placemark/{id}")
    public ResponseEntity<PlacemarkDto> getPlacemark(@PathVariable Long id) throws TechException {
        return ResponseEntity.ok(shelterService.getPlacemark(id));
    }

    @GetMapping("/get_placemark/")
    public ResponseEntity<List<PlacemarkDto>> getAllPlacemarks() throws TechException {
        return ResponseEntity.ok(shelterService.getAllPlacemarks());
    }

}
