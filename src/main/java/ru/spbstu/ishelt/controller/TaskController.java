package ru.spbstu.ishelt.controller;

import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.dto.TaskDto;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.service.TaskService;

@RestController
@RequestMapping("api/v1/tasks")
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/create_task")
    public ResponseEntity<GlobalResponse> createTask(@Valid @RequestBody TaskDto request) throws TechException {
        return ResponseEntity.ok(taskService.createTask(request));
    }

    @GetMapping("/get_task/{id}")
    public ResponseEntity<TaskDto> getTask(@PathVariable("id") Long taskId) throws TechException {
        return ResponseEntity.ok(taskService.getTask(taskId));
    }

    @GetMapping("/get_active_tasks/all")
    public ResponseEntity<Page<TaskDto>> getActiveTasks(
            @RequestParam(value = "offset", defaultValue = "0") Integer offset,
            @RequestParam(value = "limit", defaultValue = "10") Integer limit) throws TechException {
        return ResponseEntity.ok(taskService.getActiveTasks(offset, limit));
    }

    @GetMapping("/get_finished_tasks/all")
    public ResponseEntity<Page<TaskDto>> getFinishedTasks(
            @RequestParam(value = "offset", defaultValue = "0") Integer offset,
            @RequestParam(value = "limit", defaultValue = "10") Integer limit) throws TechException {
        return ResponseEntity.ok(taskService.getFinishedTasks(offset, limit));
    }

    @PutMapping("/respond")
    public ResponseEntity<GlobalResponse> respondTask(@RequestParam("task_id") Long taskId) throws  TechException {
        return ResponseEntity.ok(taskService.respondTask(taskId));
    }

    @PutMapping("/finish")
    public ResponseEntity<GlobalResponse> finishTask(@RequestParam("task_id") Long taskId,
                                                     @RequestParam("username") String performerName) throws  TechException {
        return ResponseEntity.ok(taskService.finishTask(taskId, performerName));
    }

}
