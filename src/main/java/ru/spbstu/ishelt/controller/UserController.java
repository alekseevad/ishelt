package ru.spbstu.ishelt.controller;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.spbstu.ishelt.dto.CreateAppUserRequestDto;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.dto.PostDto;
import ru.spbstu.ishelt.dto.UserProfile;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.service.UserService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("api/v1/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/create_user")
    public ResponseEntity<GlobalResponse> createUser(@Valid @RequestBody CreateAppUserRequestDto request) throws TechException {
        return ResponseEntity.ok(userService.createUser(request));
    }

    @PutMapping("/edit_user")
    public ResponseEntity<GlobalResponse> editUser(@Valid @RequestBody CreateAppUserRequestDto request) throws TechException {
        return ResponseEntity.ok(userService.editUser(request));
    }

    @PutMapping("/set_like")
    public ResponseEntity<GlobalResponse> setLike(@RequestParam("username") String username, @RequestParam("post_id") Long postId) throws TechException{
        return ResponseEntity.ok(userService.setLike(username, postId));
    }

    @PutMapping("/unlike")
    public ResponseEntity<GlobalResponse> unlike(@RequestParam("username") String username, @RequestParam("post_id") Long postId) throws TechException{
        return ResponseEntity.ok(userService.unlike(username, postId));
    }

    @GetMapping("/profile")
    public ResponseEntity<UserProfile> getProfile(@RequestParam("username") String username) throws TechException {
        return ResponseEntity.ok(userService.getProfile(username));
    }

    @GetMapping("/profile/current")
    public ResponseEntity<UserProfile> getProfileCurrent(Principal principal) throws TechException {
        return ResponseEntity.ok(userService.getProfileCurrent(principal));
    }

    @GetMapping("/profile/posts")
    public ResponseEntity<List<PostDto>> getProfilePosts(@RequestParam("username") String username) throws TechException {
        return ResponseEntity.ok(userService.getProfilePosts(username));
    }

}
