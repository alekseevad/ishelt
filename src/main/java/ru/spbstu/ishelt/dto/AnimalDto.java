package ru.spbstu.ishelt.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import ru.spbstu.ishelt.model.Animal;
import ru.spbstu.ishelt.utility.mapper.PhotoMapper;
import ru.spbstu.ishelt.utility.mapper.ShelterMapper;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnimalDto {
    private Long id;
    @Length(max = 25)
    @JsonProperty("name")
    private String name;
    @JsonProperty("shelter")
    private ShelterDto shelterDto;
    @Length(min = 1, max = 1)
    private String sex;
    private String breed;
    @Length(max = 30)
    private String coloring;
    @Min(0)
    @Max(300)
    private Integer age;
    @Min(0)
    private Double size;
    @JsonProperty("spayed_neutered")
    private Boolean spayedNeutered;
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @JsonProperty("intake_date")
    private Date intakeDate;
    @Length(max = 255)
    private String description;
    private List<PhotoDto> photos;

    public AnimalDto(Animal animal) {
        this.id = animal.getId();
        this.name = animal.getName();
        this.shelterDto = ShelterMapper.INSTANCE.toDto(animal.getShelter());
        this.sex = animal.getSex();
        this.breed = animal.getBreed();
        this.coloring = animal.getColoring();
        this.age = animal.getAge();
        this.size = animal.getSize();
        this.description = animal.getDescription();
        this.intakeDate = animal.getIntakeDate();
        this.spayedNeutered = animal.getSpayedNeutered();
        this.photos = animal.getPhotos().stream().map(PhotoMapper.INSTANCE::toDto).collect(Collectors.toList());
    }

}
