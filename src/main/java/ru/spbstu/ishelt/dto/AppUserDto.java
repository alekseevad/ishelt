package ru.spbstu.ishelt.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.spbstu.ishelt.model.Role;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppUserDto {
    private Long id;
    private String username;
    private String firstname;
    private String lastname;
    private Role role;
    private String photo;
}
