package ru.spbstu.ishelt.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateAppUserRequestDto {
    @JsonProperty("old_username")
    private String oldUsername;
    @Email(message = "Email is incorrect")
    @NotBlank(message = "New Email is blank")
    @JsonProperty("new_username")
    private String newUsername;
    @Length(min = 1, max = 25, message = "Firstname's length is incorrect")
    private String firstname;
    @Length(min = 1, max = 25, message = "Lastname's length is incorrect")
    private String lastname;
    @NotBlank(message = "Password is blank")
    @Length(min = 8, max = 16, message = "Password's length is incorrect")
    private String password;
    @NotBlank(message = "Role is blank")
    private String role;
    private String photo;

}
