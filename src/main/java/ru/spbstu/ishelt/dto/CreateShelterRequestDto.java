package ru.spbstu.ishelt.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateShelterRequestDto {
    @Length(min = 5, max = 25, message = "Shelter name is incorrect")
    private String name;
    @Length(max = 60)
    private String address;
    @Pattern(
            regexp = "^\\d{11}$",
            message = "Not matching phone number"
    )
    private String phone;
    @Email(message = "Email is incorrect")
    private String email;
    private String owner;
    private List<PhotoDto> photos;

}
