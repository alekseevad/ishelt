package ru.spbstu.ishelt.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import ru.spbstu.ishelt.model.LostAnimal;
import ru.spbstu.ishelt.utility.mapper.AnimalMapper;
import ru.spbstu.ishelt.utility.mapper.PhotoMapper;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LostAnimalDto {
    private Long id;
    @JsonProperty("animal")
    private AnimalDto animalDto;
    @Length(min = 1, max = 1)
    private String sex;
    @JsonProperty("finding_date")
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private Date findingDate;
    private String coordinates;
    @Length(max = 255)
    private String description;
    private List<PhotoDto> photos;

    public LostAnimalDto(LostAnimal lostAnimal) {
        this.id = lostAnimal.getId();
        this.animalDto = AnimalMapper.INSTANCE.toDto(lostAnimal.getAnimal());
        this.sex = lostAnimal.getSex();
        this.description = lostAnimal.getDescription();
        this.findingDate = lostAnimal.getFindingDate();
        this.coordinates = lostAnimal.getCoordinates();
        this.photos = lostAnimal.getPhotos().stream().map(PhotoMapper.INSTANCE::toDto).collect(Collectors.toList());
    }

}
