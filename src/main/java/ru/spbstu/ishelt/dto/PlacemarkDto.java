package ru.spbstu.ishelt.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PlacemarkDto {
    private Long id;
    private Double latitude;
    private Double longitude;
}
