package ru.spbstu.ishelt.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import ru.spbstu.ishelt.model.Post;
import ru.spbstu.ishelt.utility.mapper.AnimalMapper;
import ru.spbstu.ishelt.utility.mapper.AppUserMapper;
import ru.spbstu.ishelt.utility.mapper.LostAnimaLMapper;
import ru.spbstu.ishelt.utility.mapper.PhotoMapper;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {
    private Long id;
    @JsonProperty("app_user")
    private AppUserDto appUserDto;
    @JsonProperty("animal")
    private AnimalDto animalDto;
    @JsonProperty("lost_animal")
    private LostAnimalDto lostAnimalDto;
    @Length(max = 400)
    private String text;
    @CreatedDate
    private LocalDateTime creationDate;
    private Boolean status;
    private List<AppUserDto> likedUsers;
    private List<PhotoDto> photos;

    public PostDto(Post post) {
        this.id = post.getId();
        this.appUserDto = AppUserMapper.INSTANCE.toDto(post.getAppUser());
        this.animalDto = AnimalMapper.INSTANCE.toDto(post.getAnimal());
        this.lostAnimalDto = LostAnimaLMapper.INSTANCE.toDto(post.getLostAnimal());
        this.text = post.getText();
        this.creationDate = post.getCreationDate();
        this.status = post.getStatus();
        this.likedUsers = post.getLikedUsers().stream().map(AppUserMapper.INSTANCE::toDto).collect(Collectors.toList());
        this.photos = post.getPhotos().stream().map(PhotoMapper.INSTANCE::toDto).collect(Collectors.toList());
    }

}
