package ru.spbstu.ishelt.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.spbstu.ishelt.model.Shelter;
import ru.spbstu.ishelt.utility.mapper.AnimalMapper;
import ru.spbstu.ishelt.utility.mapper.AppUserMapper;
import ru.spbstu.ishelt.utility.mapper.PhotoMapper;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShelterDto {
    private Long id;
    private String name;
    private String address;
    private String phone;
    private String email;
    private List<AnimalDto> animals;
    private List<AppUserDto> owners;
    private List<PhotoDto> photos;

    public ShelterDto(Shelter shelter) {
        this.id = shelter.getId();
        this.name = shelter.getName();
        this.address = shelter.getAddress();
        this.phone = shelter.getPhone();
        this.email = shelter.getEmail();
        this.animals = shelter.getAnimals().stream().map(AnimalMapper.INSTANCE::toDto).collect(Collectors.toList());
        this.owners = shelter.getOwners().stream().map(AppUserMapper.INSTANCE::toDto).collect(Collectors.toList());
        this.photos = shelter.getPhotos().stream().map(PhotoMapper.INSTANCE::toDto).collect(Collectors.toList());
    }

}
