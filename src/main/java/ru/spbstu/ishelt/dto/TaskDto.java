package ru.spbstu.ishelt.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import ru.spbstu.ishelt.model.Task;
import ru.spbstu.ishelt.utility.mapper.AppUserMapper;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {
    private Long id;
    @JsonProperty("shelter_owner")
    private AppUserDto shelterOwner;
    @Length(max = 400)
    private String text;
    @CreatedDate
    private LocalDateTime creationDate;
    private Boolean status;
    private List<AppUserDto> responders;

    public TaskDto(Task task) {
        this.id = task.getId();
        this.shelterOwner = AppUserMapper.INSTANCE.toDto(task.getShelterOwner());
        this.text = task.getText();
        this.creationDate = task.getCreationDate();
        this.status = task.getStatus();
        this.responders = task.getResponders().stream().map(AppUserMapper.INSTANCE::toDto).collect(Collectors.toList());
    }

}
