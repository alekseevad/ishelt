package ru.spbstu.ishelt.exception;

public class TechException extends Exception {

    public TechException(String message) {
        super(message);
    }

}
