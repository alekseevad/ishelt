package ru.spbstu.ishelt.exception.handlers;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.utility.JsonUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    private static final String ANY_TECH_ERROR = "TECHNICAL ERROR";
    private static final String VALIDATION_ERROR = "VALIDATION ERROR";
    private static final String REQUEST_NOT_READABLE = "REQUEST NOT READABLE";
    Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = {TechException.class})
    public ResponseEntity<Object> handleTechException(TechException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);

        var response = new GlobalResponse(
                HttpStatus.BAD_REQUEST.value(),
                ANY_TECH_ERROR,
                ex.getMessage());

        return super.handleExceptionInternal(ex, response, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> onConstraintValidationException(ConstraintViolationException ex, WebRequest request) {
        final Map<String, String> errors = ex.getConstraintViolations().stream()
                .collect(Collectors.toMap(
                        violation -> violation.getPropertyPath().toString(),
                        ConstraintViolation::getMessage
                ));

        var response = new GlobalResponse(
                HttpStatus.BAD_REQUEST.value(),
                VALIDATION_ERROR,
                JsonUtils.toPrettyJsonString(errors)
        );

        return super.handleExceptionInternal(ex, response, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String message = error.getDefaultMessage();
            errors.put(fieldName, message);
        });

        var response = new GlobalResponse(
                HttpStatus.BAD_REQUEST.value(),
                VALIDATION_ERROR,
                JsonUtils.toPrettyJsonString(errors)
        );

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        var response = new GlobalResponse(
                HttpStatus.BAD_REQUEST.value(),
                REQUEST_NOT_READABLE,
                ex.getMessage()
        );

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
