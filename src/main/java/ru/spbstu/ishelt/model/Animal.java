package ru.spbstu.ishelt.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.validator.constraints.Length;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Builder
@Entity
@Table(name="ANIMAL")
public class Animal extends GenericEntity<Long> {
    @Column(name = "animal_name")
    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "shelter_id")
    private Shelter shelter;
    private String sex;
    private String breed;
    private String coloring;
    @Min(0)
    private Integer age;
    @Min(0)
    private Double size;
    @Column(name = "spayed_neutered")
    private Boolean spayedNeutered;
    @Column(name = "intake_date")
    private Date intakeDate;
    @Length(max = 250)
    private String description;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "animal")
    List<Photo> photos;
}
