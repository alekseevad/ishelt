package ru.spbstu.ishelt.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "LOST_ANIMAL")
public class LostAnimal extends GenericEntity<Long> {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "animal_id")
    private Animal animal;
    private String sex;
    @Column(name = "lost_animal_date")
    private Date findingDate;
    private String coordinates;
    private String description;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lostAnimal")
    List<Photo> photos;
}
