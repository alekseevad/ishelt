package ru.spbstu.ishelt.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "POST")
public class Post extends GenericEntity<Long> {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "app_user_id")
    private AppUser appUser;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "animal_id")
    private Animal animal;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lost_animal_id")
    private LostAnimal lostAnimal;
    private String text;
    @Column(name = "creation_date")
    private LocalDateTime creationDate;
    private Boolean status;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "post")
    List<Photo> photos;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "app_user_post",
            joinColumns = {@JoinColumn(name = "post_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")})
    private List<AppUser> likedUsers;

}
