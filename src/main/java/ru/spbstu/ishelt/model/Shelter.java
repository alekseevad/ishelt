package ru.spbstu.ishelt.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "SHELTER")
public class Shelter extends GenericEntity<Long> {
    private String name;
    private String address;
    private String phone;
    private String email;
    private String coordinates;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "shelter")
    List<Photo> photos;

    @OneToMany(mappedBy = "shelter",
            fetch = FetchType.LAZY)
    private List<Animal> animals;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "app_user_shelter",
            joinColumns = {@JoinColumn(name = "shelter_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")})
    private List<AppUser> owners;

}
