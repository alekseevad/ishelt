package ru.spbstu.ishelt.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "TASK")
public class Task extends GenericEntity<Long> {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "app_user_id")
    private AppUser shelterOwner;
    private String text;
    @Column(name = "creation_date")
    private LocalDateTime creationDate;
    private Boolean status;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "app_user_task",
            joinColumns = {@JoinColumn(name = "task_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")})
    private List<AppUser> responders;

}
