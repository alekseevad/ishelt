package ru.spbstu.ishelt.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.spbstu.ishelt.model.Animal;

import java.util.Date;

@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE animal SET shelter_id = :shelter_id, sex = :sex, breed = :breed, coloring = :coloring, age = :age, size = :size, intake_date = :intake_date, spayed_neutered = :spayed_neutered, description = :description, animal_name = :animal_name WHERE id = :animal_id", nativeQuery = true)
    void updateById(
            @Param("animal_id") Long animalId,
            @Param("animal_name") String name,
            @Param("age") Integer age,
            @Param("size") Double size,
            @Param("coloring") String coloring,
            @Param("description") String description,
            @Param("intake_date") Date intakeDate,
            @Param("sex") String sex,
            @Param("breed") String breed,
            @Param("shelter_id") Long shelterId,
            @Param("spayed_neutered") Boolean spayedNeutered);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE animal SET shelter_id = null WHERE id = :animal_id", nativeQuery = true)
    void removeShelterFromAnimal(@Param("animal_id") Long animalId);

}
