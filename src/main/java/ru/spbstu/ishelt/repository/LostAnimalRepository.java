package ru.spbstu.ishelt.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.spbstu.ishelt.model.LostAnimal;

import java.util.Date;

@Repository
public interface LostAnimalRepository extends JpaRepository<LostAnimal, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE LOST_ANIMAL SET animal_id = :animal_id, sex = :sex, lost_animal_date = :finding_date, coordinates = :coordinates, description = :description WHERE id = :lost_animal_id", nativeQuery = true)
    void updateById(
            @Param("lost_animal_id") Long lostAnimalId,
            @Param("animal_id") Long animalId,
            @Param("sex") String sex,
            @Param("finding_date") Date findingDate,
            @Param("coordinates") String coordinates,
            @Param("description") String description);

}
