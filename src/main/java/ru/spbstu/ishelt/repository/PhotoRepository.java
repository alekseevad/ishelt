package ru.spbstu.ishelt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.spbstu.ishelt.model.Photo;

@Repository
public interface PhotoRepository extends JpaRepository<Photo, Long> {
}
