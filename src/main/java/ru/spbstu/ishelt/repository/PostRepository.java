package ru.spbstu.ishelt.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.spbstu.ishelt.model.Post;

import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    Optional<Page<Post>> findAllByStatusTrue(PageRequest of);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE POST SET animal_id = :animal_id, lost_animal_id = :lost_animal_id, text = :text WHERE id = :post_id",nativeQuery = true)
    void updateById(@Param("post_id") Long postId,
                    @Param("animal_id") Long animalId,
                    @Param("lost_animal_id") Long lostAnimalId,
                    @Param("text") String text);
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE POST SET status = false WHERE id = :post_id", nativeQuery = true)
    void deleteById(@Param("post_id") Long postId);

}
