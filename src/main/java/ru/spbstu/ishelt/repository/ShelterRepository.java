package ru.spbstu.ishelt.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.spbstu.ishelt.model.Shelter;

import java.util.Optional;

@Repository
public interface ShelterRepository extends JpaRepository<Shelter, Long> {

    Optional<Shelter> findShelterByName(String name);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE SHELTER SET name = :name, address = :address, phone = :phone, email= :email WHERE id= :id", nativeQuery = true)
    void updateById(@Param("id") Long shelterId,
                                 @Param("name") String name,
                                 @Param("address") String address,
                                 @Param("phone") String phone,
                                 @Param("email") String email);

}
