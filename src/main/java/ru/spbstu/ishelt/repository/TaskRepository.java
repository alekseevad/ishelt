package ru.spbstu.ishelt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.spbstu.ishelt.model.Task;

import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    Optional<Page<Task>> findAllByStatusFalse(PageRequest of);

    Optional<Page<Task>> findAllByStatusTrue(PageRequest of);

}
