package ru.spbstu.ishelt.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.spbstu.ishelt.model.AppUser;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<AppUser, Long> {

    Optional<AppUser> findByUsername(String login);

    Boolean existsByUsername(String username);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE APP_USER SET username = :new_username, firstname = :firstname, lastname = :lastname, password= :password, photo = :photo, role_id= :role WHERE username= :old_username", nativeQuery = true)
    void updateByUsername(@Param("old_username") String oldUsername,
                                       @Param("new_username") String newUsername,
                                       @Param("firstname") String firstname,
                                       @Param("lastname") String lastname,
                                       @Param("photo") String photo,
                                       @Param("password") String password,
                                       @Param("role") Long role);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE APP_USER SET status = 'BANNED' WHERE username = :username", nativeQuery = true)
    void banByUsername(@Param("username") String username);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE APP_USER SET role_id = (SELECT id FROM ROLE WHERE name = :role) WHERE username = :username", nativeQuery = true)
    void updateRole(@Param("username")String username,
                    @Param("role") String role);
}
