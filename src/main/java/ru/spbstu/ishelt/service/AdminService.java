package ru.spbstu.ishelt.service;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.repository.UserRepository;

@AllArgsConstructor
@Service
public class AdminService {
    private final UserRepository userRepository;

    public GlobalResponse banUser(String username) throws TechException {
        try{
            if(userRepository.existsByUsername(username)){
                userRepository.banByUsername(username);
            }

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("User banned")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

}
