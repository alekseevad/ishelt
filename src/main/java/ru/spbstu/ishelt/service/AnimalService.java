package ru.spbstu.ishelt.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.spbstu.ishelt.dto.AnimalDto;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.model.Animal;
import ru.spbstu.ishelt.model.Photo;
import ru.spbstu.ishelt.model.Shelter;
import ru.spbstu.ishelt.repository.AnimalRepository;
import ru.spbstu.ishelt.repository.PhotoRepository;
import ru.spbstu.ishelt.repository.ShelterRepository;
import ru.spbstu.ishelt.utility.mapper.PhotoMapper;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@Service
public class AnimalService {
    private final AnimalRepository animalRepository;
    private final ShelterRepository shelterRepository;
    private final PhotoRepository photoRepository;

    public GlobalResponse createAnimal(AnimalDto request) throws TechException{
        try {
            Shelter shelter = null;
            if(request.getShelterDto() != null) {
                shelter = shelterRepository.findShelterByName(request.getShelterDto().getName())
                        .orElseThrow(() -> new TechException("Shelter with name " + request.getShelterDto().getName() + " not Found"));
            }

            List<Photo> photos = null;
            if(request.getPhotos() != null) {
                photos = request.getPhotos().stream().map(PhotoMapper.INSTANCE::toEntity).collect(Collectors.toList());
            }

            var animal = Animal.builder()
                    .name(request.getName())
                    .age(request.getAge())
                    .sex(request.getSex())
                    .breed(request.getBreed())
                    .coloring(request.getColoring())
                    .size(request.getSize())
                    .intakeDate(request.getIntakeDate())
                    .spayedNeutered(request.getSpayedNeutered())
                    .shelter(shelter)
                    .description(request.getDescription())
                    .photos(photos)
                    .build();
            log.info("Created animal: " + animal);
            animalRepository.save(animal);
            if(photos != null) {
                photos.forEach(e -> e.setAnimal(animal));
                photoRepository.saveAll(photos);
            }

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.CREATED.value())
                    .message("Animal created successfully")
                    .build();
        }
        catch(Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public Page<AnimalDto> getAnimals(Integer offset, Integer limit) throws TechException {
        try{
            Page<Animal> animals = animalRepository.findAll(PageRequest.of(offset, limit));
            return animals.map(AnimalDto::new);
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public AnimalDto getAnimal(Long id) throws TechException {
        try {
            var animal = animalRepository.findById(id)
                    .orElseThrow(() -> new TechException("Animal with id " + id + " not Found"));
            return new AnimalDto(animal);
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public GlobalResponse editAnimal(Long animalId, AnimalDto request) throws TechException{
        try {
            animalRepository.updateById(
                    animalId,
                    request.getName(),
                    request.getAge(),
                    request.getSize(),
                    request.getColoring(),
                    request.getDescription(),
                    request.getIntakeDate(),
                    request.getSex(),
                    request.getBreed(),
                    request.getShelterDto().getId(),
                    request.getSpayedNeutered()
            );

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Animal edited successfully")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

}
