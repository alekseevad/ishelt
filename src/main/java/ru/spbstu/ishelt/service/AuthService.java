package ru.spbstu.ishelt.service;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import ru.spbstu.ishelt.dto.AuthRequestDto;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.security.JwtTokenProvider;

@AllArgsConstructor
@Service
public class AuthService {
    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final JwtTokenProvider jwtTokenProvider;

    public GlobalResponse login(AuthRequestDto request) throws TechException {
        try{
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getUsername(),
                            request.getPassword()
                    )
            );
            var user = userService.findByUsername(request.getUsername()).orElseThrow();
            var jwtToken = jwtTokenProvider.generateToken(user);
            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Authorization successful")
                    .details(jwtToken)
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

}
