package ru.spbstu.ishelt.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.dto.LostAnimalDto;
import ru.spbstu.ishelt.dto.PlacemarkDto;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.model.Animal;
import ru.spbstu.ishelt.model.LostAnimal;
import ru.spbstu.ishelt.model.Photo;
import ru.spbstu.ishelt.repository.AnimalRepository;
import ru.spbstu.ishelt.repository.LostAnimalRepository;
import ru.spbstu.ishelt.repository.PhotoRepository;
import ru.spbstu.ishelt.utility.mapper.PhotoMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@Service
public class LostAnimalService {
    private final LostAnimalRepository lostAnimalRepository;
    private final AnimalRepository animalRepository;
    private final PhotoRepository photoRepository;

    public GlobalResponse createLostAnimal(LostAnimalDto request) throws TechException {
        try {
            Animal animal = null;
            if(request.getAnimalDto() != null) {
                animal = animalRepository.findById(request.getAnimalDto().getId())
                        .orElseThrow(() -> new TechException("Animal with id " + request.getAnimalDto().getId() + " not found"));
            }

            List<Photo> photos = null;
            if(request.getPhotos() != null) {
                photos = request.getPhotos().stream().map(PhotoMapper.INSTANCE::toEntity).collect(Collectors.toList());
            }

            var lostAnimal = LostAnimal.builder()
                    .animal(animal)
                    .sex(request.getSex())
                    .description(request.getDescription())
                    .coordinates(request.getCoordinates())
                    .findingDate(request.getFindingDate())
                    .photos(photos)
                    .build();
            log.info("Created lost animal: " + lostAnimal);
            lostAnimalRepository.save(lostAnimal);
            if(photos != null) {
                photos.forEach(e -> e.setLostAnimal(lostAnimal));
                photoRepository.saveAll(photos);
            }

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.CREATED.value())
                    .message("Lost animal created successfully")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public LostAnimalDto getLostAnimal(Long id) throws TechException {
        try {
            var lostAnimal = lostAnimalRepository.findById(id)
                    .orElseThrow(() -> new TechException("Animal with id  " + id + " not Found"));
            return new LostAnimalDto(lostAnimal);
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public Page<LostAnimalDto> getLostAnimals(Integer offset, Integer limit) throws TechException {
        try {
            Page<LostAnimal> lostAnimals = lostAnimalRepository.findAll(PageRequest.of(offset, limit));
            return lostAnimals.map(LostAnimalDto::new);
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public GlobalResponse editLostAnimal(Long lostAnimalId, LostAnimalDto request) throws TechException {
        try {
            lostAnimalRepository.updateById(
                    lostAnimalId,
                    request.getAnimalDto().getId(),
                    request.getSex(),
                    request.getFindingDate(),
                    request.getCoordinates(),
                    request.getDescription()
            );

            return GlobalResponse
                    .builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Lost Animal " + lostAnimalId + " edited successfully")
                    .build();

        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public GlobalResponse boundWithAnimal(Long lostAnimalId, Long animalId) throws TechException {
        try {
            var lostAnimal = lostAnimalRepository.findById(lostAnimalId)
                    .orElseThrow(() -> new TechException("Lost Animal with id  " + lostAnimalId + " not Found"));

            lostAnimalRepository.updateById(
                    lostAnimalId,
                    animalId,
                    lostAnimal.getSex(),
                    lostAnimal.getFindingDate(),
                    lostAnimal.getCoordinates(),
                    lostAnimal.getDescription()
            );

            return GlobalResponse
                    .builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Lost animal, id = " + lostAnimalId + " bounded with animal, id = " + animalId)
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public PlacemarkDto getPlacemark(Long id) throws TechException {
        try {
            var lostAnimal = lostAnimalRepository.findById(id)
                    .orElseThrow(() -> new TechException("Lost Animal with id  " + id + " not Found"));

            String coordinates = lostAnimal.getCoordinates();
            Double latitude = null;
            Double longitude = null;
            if(coordinates != null) {
                String[] split = coordinates.split(" ");
                latitude = Double.parseDouble(split[0]);
                longitude = Double.parseDouble(split[1]);
            }

            return PlacemarkDto
                    .builder()
                    .id(id)
                    .latitude(latitude)
                    .longitude(longitude)
                    .build();

        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }


    public List<PlacemarkDto> getAllPlacemarks() throws TechException {
        try {
            List<LostAnimal> animals = lostAnimalRepository.findAll();
            List<PlacemarkDto> placemarkList = new ArrayList<>();

            for(LostAnimal lostAnimal : animals) {
                String coordinates = lostAnimal.getCoordinates();

                if(coordinates != null) {
                    String[] split = coordinates.split(" ");
                    Double latitude  = Double.parseDouble(split[0]);
                    Double longitude = Double.parseDouble(split[1]);
                    placemarkList.add(
                            PlacemarkDto
                                    .builder()
                                    .id(lostAnimal.getId())
                                    .latitude(latitude)
                                    .longitude(longitude)
                                    .build());
                }
            }

            return placemarkList;
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }
}
