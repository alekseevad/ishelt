package ru.spbstu.ishelt.service;

import io.minio.GetObjectArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.exception.TechException;

import java.util.Random;

@Slf4j
@RequiredArgsConstructor
@Service
public class MinioService {
    private final MinioClient minioClient;
    @Value("${minio.bucket-name}")
    private String bucketName;

    public GlobalResponse uploadFile(MultipartFile file) throws TechException {
        try {
            String fileName = System.currentTimeMillis() + new Random().nextInt() + "_" + file.getOriginalFilename();
            log.info("Filename: " + fileName);
            minioClient.putObject(
                    PutObjectArgs.builder()
                            .bucket(bucketName)
                            .object(fileName)
                            .stream(file.getInputStream(), file.getSize(), -1)
                            .contentType(file.getContentType())
                            .build()
            );

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("File uploaded successfully")
                    .details(fileName)
                    .build();

        } catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public byte[] downloadFile(String fileName) throws TechException {
        try {
            return minioClient.getObject(
                    GetObjectArgs.builder()
                            .bucket(bucketName)
                            .object(fileName)
                            .build()
            ).readAllBytes();
        } catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

}

