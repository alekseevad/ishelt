package ru.spbstu.ishelt.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.dto.PostDto;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.model.*;
import ru.spbstu.ishelt.repository.*;
import ru.spbstu.ishelt.utility.mapper.PhotoMapper;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@Service
public class PostService {
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final AnimalRepository animalRepository;
    private final LostAnimalRepository lostAnimalRepository;
    private final PhotoRepository photoRepository;

    public GlobalResponse createPost(PostDto request) throws TechException {
        try {
            AppUser appUser = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName())
                    .orElseThrow(() -> new TechException("User with username " + request.getAppUserDto().getUsername() + " not found"));
            Animal animal = null;
            if(request.getAnimalDto() != null) {
                animal = animalRepository.findById(request.getAnimalDto().getId())
                        .orElseThrow(() -> new TechException("Animal with id " + request.getAnimalDto().getId() + " not found"));
            }
            LostAnimal lostAnimal = null;
            if(request.getLostAnimalDto() != null) {
                lostAnimal = lostAnimalRepository.findById(request.getLostAnimalDto().getId())
                        .orElseThrow(() -> new TechException("Lost animal with id " + request.getLostAnimalDto().getId() + " not found"));
            }
            List<Photo> photos = null;
            if(request.getPhotos() != null) {
                photos = request.getPhotos().stream().map(PhotoMapper.INSTANCE::toEntity).collect(Collectors.toList());
            }

            var post = Post.builder()
                    .appUser(appUser)
                    .animal(animal)
                    .lostAnimal(lostAnimal)
                    .text(request.getText())
                    .creationDate(LocalDateTime.now())
                    .status(true)
                    .photos(photos)
                    .build();
            log.info("Created post: " + post);
            postRepository.save(post);
            if(photos != null) {
                photos.forEach(e -> e.setPost(post));
                photoRepository.saveAll(photos);
            }

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Post created successfully")
                    .build();
        }
        catch (Exception e){
            throw new TechException(e.getMessage());
        }
    }

    public PostDto getPost(Long postId) throws TechException {
        try {
            var post = postRepository.findById(postId)
                    .orElseThrow(() -> new TechException("Post with id " + postId + " not found"));
            return new PostDto(post);
        }
        catch (Exception e){
            throw new TechException(e.getMessage());
        }
    }

    public Page<PostDto> getPosts(Integer offset, Integer limit) throws TechException {
        try {
            Page<Post> posts = postRepository.findAllByStatusTrue(PageRequest.of(offset, limit))
                    .orElseThrow(() -> new TechException("No posts found"));

            return posts.map(PostDto::new);
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public GlobalResponse editPost(PostDto request) throws TechException {
        try {
            postRepository.updateById(
                    request.getId(),
                    request.getAnimalDto().getId(),
                    request.getLostAnimalDto().getId(),
                    request.getText()
            );

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Post edited successfully")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public GlobalResponse deletePost(Long postId) throws TechException {
        try {
            postRepository.deleteById(postId);

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Post deleted successfully")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

}
