package ru.spbstu.ishelt.service;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.spbstu.ishelt.dto.CreateShelterRequestDto;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.dto.PlacemarkDto;
import ru.spbstu.ishelt.dto.ShelterDto;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.model.Animal;
import ru.spbstu.ishelt.model.AppUser;
import ru.spbstu.ishelt.model.Photo;
import ru.spbstu.ishelt.model.Shelter;
import ru.spbstu.ishelt.repository.AnimalRepository;
import ru.spbstu.ishelt.repository.PhotoRepository;
import ru.spbstu.ishelt.repository.ShelterRepository;
import ru.spbstu.ishelt.repository.UserRepository;
import ru.spbstu.ishelt.utility.mapper.PhotoMapper;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@Service
public class ShelterService {
    private final ShelterRepository shelterRepository;
    private final AnimalRepository animalRepository;
    private final UserRepository userRepository;
    private final PhotoRepository photoRepository;

    public GlobalResponse createShelter(CreateShelterRequestDto request) throws TechException {
        try {
            AppUser owner = userRepository.findByUsername(request.getOwner())
                    .orElseThrow(() -> new TechException("User with username " + request.getOwner() + " not found"));

            List<Photo> photos = null;
            if(request.getPhotos() != null) {
                photos = request.getPhotos().stream().map(PhotoMapper.INSTANCE::toEntity).collect(Collectors.toList());
            }

            var shelter = Shelter.builder()
                    .name(request.getName())
                    .address(request.getAddress())
                    .phone(request.getPhone())
                    .email(request.getEmail())
                    .owners(Collections.singletonList(owner))
                    .photos(photos)
                    .build();
            log.info("Created shelter: " + shelter);
            shelterRepository.save(shelter);
            userRepository.updateRole(owner.getUsername(), "SHELTER_OWNER");
            if(photos != null) {
                photos.forEach(e -> e.setShelter(shelter));
                photoRepository.saveAll(photos);
            }

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.CREATED.value())
                    .message("Shelter created successfully")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public Shelter getShelterById(Long id) throws TechException {
        try {
            return shelterRepository.findById(id)
                    .orElseThrow(() -> new TechException("Shelter with id " + id + " not found"));
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public ShelterDto getShelterByName(String name) throws TechException {
        try {
            var shelter = shelterRepository.findShelterByName(name)
                    .orElseThrow(() -> new TechException("Shelter with name " + name + " not found"));
            return new ShelterDto(shelter);
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public Page<ShelterDto> getShelters(Integer offset, Integer limit) throws TechException {
        try {
            Page<Shelter> shelters = shelterRepository.findAll(PageRequest.of(offset, limit));
            return shelters.map(ShelterDto::new);
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public GlobalResponse editShelter(String shelterName, CreateShelterRequestDto request) throws TechException {
        try {

            var shelter = shelterRepository.findShelterByName(shelterName)
                    .orElseThrow(() -> new TechException("Shelter with name " + shelterName + " not found"));
            shelterRepository.updateById(
                    shelter.getId(),
                    request.getName(),
                    request.getAddress(),
                    request.getPhone(),
                    request.getEmail()
            );

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Shelter edited")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    @Transactional
    public GlobalResponse addUser(String shelterName, String username) throws TechException {
        try{

            AppUser user = userRepository.findByUsername(username)
                    .orElseThrow(() -> new TechException("User with username " + username + " not found"));
            Shelter shelter = shelterRepository.findShelterByName(shelterName).orElseThrow(() -> new RuntimeException("No such shelter"));

            List<AppUser> owners = shelter.getOwners();
            if(!owners.contains(user)) {
                owners.add(user);
                shelter.setOwners(owners);
                shelterRepository.save(shelter);
            }
            userRepository.updateRole(username, "SHELTER_OWNER");
            log.info("Shelter owners: " + shelter.getOwners());

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("New shelter owner added to shelter")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    @Transactional
    public GlobalResponse addAnimal(String shelterName, Long animalId, Principal principal) throws TechException {
        try{
            Optional<AppUser> shelterOwner = userRepository.findByUsername(principal.getName());

            Shelter shelter = shelterRepository.findShelterByName(shelterName)
                    .orElseThrow(() -> new TechException("Shelter with name " + shelterName + " not found"));

            log.info("Shelter owners: " + shelter.getOwners());
            if(shelterOwner.isEmpty() || !shelter.getOwners().contains(shelterOwner.get())) {
                log.debug("User(not shelter owner): " + shelterOwner);
                throw new TechException("Not the owner");
            }

            Animal animal = animalRepository.findById(animalId)
                    .orElseThrow(() -> new TechException("Animal with id " + animalId + " not found"));

            animal.setShelter(shelter);
            shelter.getAnimals().add(animal);
            animalRepository.save(animal);
            shelterRepository.save(shelter);
            log.info("Sheltered animals: " + shelter.getAnimals());

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Animal added to shelter")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    @Transactional
    public GlobalResponse removeAnimal(Long animalId) throws TechException {
        try {
            Animal animal = animalRepository.findById(animalId).orElseThrow(() -> new TechException("Animal with id " + animalId + " not found"));

            Shelter shelter = animal.getShelter();
            if(shelter != null) {
                List<Animal> animalList = shelter.getAnimals();
                animalList.remove(animal);
                shelter.setAnimals(animalList);
                shelterRepository.save(shelter);
                animalRepository.removeShelterFromAnimal(animal.getId());
                log.info("Sheltered animals: " + shelter.getAnimals());
            }

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Animal removed from shelter")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public PlacemarkDto getPlacemark(Long id) throws TechException {
        try {
            var shelter = shelterRepository.findById(id)
                    .orElseThrow(() -> new TechException("Shelter with id  " + id + " not Found"));

            String coordinates = shelter.getCoordinates();
            Double latitude = null;
            Double longitude = null;
            if(coordinates != null) {
                String[] split = coordinates.split(" ");
                latitude = Double.parseDouble(split[0]);
                longitude = Double.parseDouble(split[1]);
            }

            return PlacemarkDto
                    .builder()
                    .id(id)
                    .latitude(latitude)
                    .longitude(longitude)
                    .build();

        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public List<PlacemarkDto> getAllPlacemarks() throws TechException {
        try {
            List<Shelter> shelters = shelterRepository.findAll();
            List<PlacemarkDto> placemarkList = new ArrayList<>();

            for(Shelter shelter : shelters) {
                String coordinates = shelter.getCoordinates();

                if(coordinates != null) {
                    String[] split = coordinates.split(" ");
                    Double latitude  = Double.parseDouble(split[0]);
                    Double longitude = Double.parseDouble(split[1]);
                    placemarkList.add(
                            PlacemarkDto
                                    .builder()
                                    .id(shelter.getId())
                                    .latitude(latitude)
                                    .longitude(longitude)
                                    .build());
                }
            }

            return placemarkList;
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

}
