package ru.spbstu.ishelt.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.dto.TaskDto;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.model.AppUser;
import ru.spbstu.ishelt.model.Task;
import ru.spbstu.ishelt.repository.TaskRepository;
import ru.spbstu.ishelt.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class TaskService {
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;

    public GlobalResponse createTask(TaskDto request) throws TechException {
        try {
            AppUser shelterOwner = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName())
                    .orElseThrow(() -> new TechException("User with username " + request.getShelterOwner().getUsername() + " not found"));

            var task = Task.builder()
                    .shelterOwner(shelterOwner)
                    .text(request.getText())
                    .creationDate(LocalDateTime.now())
                    .status(false)
                    .build();
            log.info("Created task: " + task.toString());
            taskRepository.save(task);

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.CREATED.value())
                    .message("Task created successfully")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public TaskDto getTask(Long taskId) throws TechException {
        try {
            var task = taskRepository.findById(taskId)
                    .orElseThrow(() -> new TechException("Task with id " + taskId + " not found"));
            return new TaskDto(task);
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public Page<TaskDto> getActiveTasks(Integer offset, Integer limit) throws TechException {
        try {
            Page<Task> tasks = taskRepository.findAllByStatusFalse(PageRequest.of(offset, limit))
                    .orElseThrow(() -> new TechException("No tasks found"));
            return tasks.map(TaskDto::new);
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public Page<TaskDto> getFinishedTasks(Integer offset, Integer limit) throws TechException {
        try {
            Page<Task> tasks = taskRepository.findAllByStatusTrue(PageRequest.of(offset, limit))
                    .orElseThrow(() -> new TechException("No tasks found"));
            return tasks.map(TaskDto::new);
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public GlobalResponse respondTask(Long taskId) throws TechException {
        try {
            AppUser newResponder = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName())
                    .orElseThrow(() -> new TechException("User not found"));
            Task task = taskRepository.findById(taskId)
                    .orElseThrow(() -> new TechException("Task with id" + taskId + "not found"));
            
            if(!task.getResponders().contains(newResponder) && !task.getStatus()) {
                List<AppUser> responders = task.getResponders();
                responders.add(newResponder);
                task.setResponders(responders);
                taskRepository.save(task);
                userRepository.updateRole(newResponder.getUsername(), "VOLUNTEER");
                log.info("Responder role: " + newResponder.getRole().getName());
            }

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.CREATED.value())
                    .message("New responder accepted")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public GlobalResponse finishTask(Long taskId, String performerName) throws TechException {
        try {
            AppUser performer = userRepository.findByUsername(performerName)
                    .orElseThrow(() -> new TechException("User with username " + performerName + " not found"));
            Task task = taskRepository.findById(taskId)
                    .orElseThrow(() -> new TechException("Task with id" + taskId + "not found"));

            if(task.getResponders().contains(performer)) {
                List<AppUser> responders = task.getResponders();
                responders.removeIf((e) -> e != performer);
                task.setResponders(responders);
                task.setStatus(true);
                taskRepository.save(task);
                log.info("Responders: " + responders);
            }
            log.info("Task status: " + task.getStatus().toString());

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.CREATED.value())
                    .message("Task finished")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

}
