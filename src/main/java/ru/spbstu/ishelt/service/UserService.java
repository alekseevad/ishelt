package ru.spbstu.ishelt.service;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.spbstu.ishelt.dto.CreateAppUserRequestDto;
import ru.spbstu.ishelt.dto.GlobalResponse;
import ru.spbstu.ishelt.dto.PostDto;
import ru.spbstu.ishelt.dto.UserProfile;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.model.AppUser;
import ru.spbstu.ishelt.model.Post;
import ru.spbstu.ishelt.model.Role;
import ru.spbstu.ishelt.repository.PostRepository;
import ru.spbstu.ishelt.repository.RoleRepository;
import ru.spbstu.ishelt.repository.UserRepository;
import ru.spbstu.ishelt.security.JwtTokenProvider;
import ru.spbstu.ishelt.utility.mapper.PostMapper;

import javax.management.relation.RoleNotFoundException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@Service
public class UserService {
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final RoleRepository roleRepository;

    public GlobalResponse createUser(CreateAppUserRequestDto request) throws TechException {
        try {
            Role role = roleRepository.findByName(request.getRole()).orElseThrow(RoleNotFoundException::new);

            var user = AppUser.builder()
                    .role(role)
                    .username(request.getNewUsername())
                    .password(passwordEncoder.encode(request.getPassword()))
                    .status("ACTIVE")
                    .createdDate(LocalDateTime.now())
                    .build();
            userRepository.save(user);
            log.info("Created user: " + user);
            var jwtToken = jwtTokenProvider.generateToken(user);
            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("New user created successfully")
                    .details(jwtToken)
                    .build();
        }
        catch (RoleNotFoundException e) {
            throw new TechException(String.format("Role with name %s not found", request.getRole()));
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public GlobalResponse editUser(CreateAppUserRequestDto request) throws TechException {
        try {
            Role role = roleRepository.findByName(request.getRole()).orElseThrow(RoleNotFoundException::new);

            userRepository.updateByUsername(
                    request.getOldUsername(),
                    request.getNewUsername(),
                    request.getFirstname(),
                    request.getLastname(),
                    request.getPhoto(),
                    passwordEncoder.encode(request.getPassword()),
                    role.getId()
            );
            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("User edited")
                    .build();
        }
        catch (RoleNotFoundException e) {
            throw new TechException(String.format("Role with name %s not found", request.getRole()));
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public Optional<AppUser> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Transactional
    public GlobalResponse setLike(String username, Long postId) throws TechException {
        try {
            AppUser user = findByUsername(username)
                    .orElseThrow(() -> new TechException("User with username " + username + " not found"));
            Post post = postRepository.findById(postId)
                    .orElseThrow(() -> new TechException("Post with id " + postId + " not found"));

            List<AppUser> likedUsers = post.getLikedUsers();
            likedUsers.add(user);
            post.setLikedUsers(likedUsers);

            userRepository.saveAll(likedUsers);
            postRepository.save(post);
            log.info("User likes: " + user.getLikedPosts().toString());

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Post liked")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public GlobalResponse unlike(String username, Long postId) throws TechException {
        try {
            AppUser user = findByUsername(username)
                    .orElseThrow(() -> new TechException("User with username " + username + " not found"));
            Post post = postRepository.findById(postId)
                    .orElseThrow(() -> new TechException("Post with id " + postId + " not found"));

            List<AppUser> likedUsers = post.getLikedUsers();
            likedUsers.remove(user);
            post.setLikedUsers(likedUsers);

            userRepository.saveAll(likedUsers);
            postRepository.save(post);
            log.info("User likes: " + user.getLikedPosts().toString());

            return GlobalResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Post unliked")
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public UserProfile getProfile(String username) throws TechException {
        try {
            AppUser user = findByUsername(username)
                    .orElseThrow(() -> new TechException("User with username " + username + " not found"));

            return UserProfile.builder()
                    .username(user.getUsername())
                    .firstname(user.getFirstname())
                    .lastname(user.getLastname())
                    .photo(user.getPhoto())
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public List<PostDto> getProfilePosts(String username) throws TechException {
        try {
            AppUser user = findByUsername(username)
                    .orElseThrow(() -> new TechException("User with username " + username + " not found"));

            return user.getOwnedPosts().stream()
                    .map(PostMapper.INSTANCE::toDto)
                    .collect(Collectors.toList());
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

    public UserProfile getProfileCurrent(Principal principal) throws TechException {
        try {
            AppUser user = userRepository.findByUsername(principal.getName())
                    .orElseThrow(() -> new TechException("User with principal " + principal + " not found"));

            return UserProfile.builder()
                    .username(user.getUsername())
                    .firstname(user.getFirstname())
                    .lastname(user.getLastname())
                    .photo(user.getPhoto())
                    .build();
        }
        catch (Exception e) {
            throw new TechException(e.getMessage());
        }
    }

}
