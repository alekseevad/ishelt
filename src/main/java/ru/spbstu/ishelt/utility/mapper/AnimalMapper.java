package ru.spbstu.ishelt.utility.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.spbstu.ishelt.dto.AnimalDto;
import ru.spbstu.ishelt.model.Animal;

@Mapper
public interface AnimalMapper {

    AnimalMapper INSTANCE = Mappers.getMapper(AnimalMapper.class);

    AnimalDto toDto(Animal animal);

    Animal toEntity(AnimalDto animalDto);

}
