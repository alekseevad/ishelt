package ru.spbstu.ishelt.utility.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.spbstu.ishelt.dto.AppUserDto;
import ru.spbstu.ishelt.model.AppUser;

@Mapper
public interface AppUserMapper {

    AppUserMapper INSTANCE = Mappers.getMapper(AppUserMapper.class);

    AppUserDto toDto(AppUser appUser);

    AppUser toEntity(AppUserDto appUserDto);

}
