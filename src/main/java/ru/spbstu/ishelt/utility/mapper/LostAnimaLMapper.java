package ru.spbstu.ishelt.utility.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.spbstu.ishelt.dto.LostAnimalDto;
import ru.spbstu.ishelt.model.LostAnimal;

@Mapper
public interface LostAnimaLMapper {

    LostAnimaLMapper INSTANCE = Mappers.getMapper(LostAnimaLMapper.class);

    LostAnimalDto toDto(LostAnimal lostAnimal);

    LostAnimal toEntity(LostAnimalDto lostAnimalDto);

}
