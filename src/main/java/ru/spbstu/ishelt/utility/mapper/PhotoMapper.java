package ru.spbstu.ishelt.utility.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.spbstu.ishelt.dto.PhotoDto;
import ru.spbstu.ishelt.model.Photo;

@Mapper
public interface PhotoMapper {

    PhotoMapper INSTANCE = Mappers.getMapper(PhotoMapper.class);

    PhotoDto toDto(Photo photo);

    Photo toEntity(PhotoDto photoDto);
}
