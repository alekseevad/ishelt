package ru.spbstu.ishelt.utility.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.spbstu.ishelt.dto.PostDto;
import ru.spbstu.ishelt.model.Post;

@Mapper
public interface PostMapper {

    PostMapper INSTANCE = Mappers.getMapper(PostMapper.class);

    PostDto toDto(Post post);

    Post toEntity(PostDto postDto);

}
