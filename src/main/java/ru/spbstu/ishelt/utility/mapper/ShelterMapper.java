package ru.spbstu.ishelt.utility.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.spbstu.ishelt.dto.ShelterDto;
import ru.spbstu.ishelt.model.Shelter;

@Mapper
public interface ShelterMapper {

    ShelterMapper INSTANCE = Mappers.getMapper(ShelterMapper.class);

    ShelterDto toDto(Shelter shelter);

    Shelter toEntity(ShelterDto shelterDto);

}
