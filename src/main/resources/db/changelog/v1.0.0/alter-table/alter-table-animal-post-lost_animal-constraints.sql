ALTER TABLE animal
    ALTER COLUMN shelter_id DROP NOT NULL;

ALTER TABLE animal
    ADD COLUMN animal_name VARCHAR(25);

ALTER TABLE lost_animal
    ALTER COLUMN animal_id DROP NOT NULL;

ALTER TABLE post
    ALTER COLUMN app_user_id DROP NOT NULL;

ALTER TABLE post
    ALTER COLUMN animal_id DROP NOT NULL;

ALTER TABLE post
    ALTER COLUMN lost_animal_id DROP NOT NULL;

