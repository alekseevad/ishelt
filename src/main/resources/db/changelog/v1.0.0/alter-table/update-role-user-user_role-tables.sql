ALTER TABLE role
    ADD CONSTRAINT role_name_unq UNIQUE(name);

ALTER TABLE app_user
    ADD CONSTRAINT username_unq UNIQUE(username);

ALTER TABLE app_user
    ADD CONSTRAINT password_unq UNIQUE(password);


