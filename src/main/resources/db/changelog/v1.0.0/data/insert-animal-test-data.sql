INSERT INTO lost_animal (animal_id, sex, finding_date, coordinates, description)
VALUES (null, 'm', '2023-01-01', '12.51 89.3', 'test lost animal');

INSERT INTO animal (shelter_id, sex, breed, coloring, age, size, spayed_neutered, intake_date, description, animal_name)
VALUES (null, 'm', 'test breed', 'test colour', 1, 23.0, true, '2023-03-05', 'test description', 'test name');

INSERT INTO animal (shelter_id, sex, breed, coloring, age, size, spayed_neutered, intake_date, description, animal_name)
VALUES (1, 'w', 'test breed', 'test colour', 1, 23.0, true, '2023-03-05', 'test shelter description', 'test shelter name');
