INSERT INTO post(app_user_id, animal_id, lost_animal_id, text, creation_date, status)
VALUES (2, 1, null, 'post text for test animal 1, posted by user 2', '2023-05-03', true);

INSERT INTO post(app_user_id, animal_id, lost_animal_id, text, creation_date, status)
VALUES (3, 2, null, 'post text for test animal 2, posted by shelterowner', '2023-05-03', true);

INSERT INTO post(app_user_id, animal_id, lost_animal_id, text, creation_date, status)
VALUES (2, null, 1, 'post text for test lost_animal 1, posted by user 2', '2023-05-03', true);