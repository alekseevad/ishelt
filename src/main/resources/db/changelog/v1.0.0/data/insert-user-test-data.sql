

INSERT INTO app_user (firstname, lastname, username, password, created_date, status)
    VALUES ('admin', 'admin', 'admin@gmail.com', '$2a$10$Qq7QueQ7KDMtngaZvpsKKeV60y0kLYco7YFH1vIniJa88Ur03B6.O', '2023-03-05', 'ACTIVE');

INSERT INTO app_user_role
    VALUES (1, 1);

INSERT INTO app_user (firstname, lastname, username, password, created_date, status)
    VALUES ('user', 'user', 'user@gmail.com', '$2a$10$/r3CQ5b7Oip0ASXgfBYNRuaHuMer.V0g6JW2lIo4i5RyPpc5C7y7G', '2023-03-05', 'ACTIVE');

INSERT INTO app_user_role
    VALUES (2, 2);

INSERT INTO app_user (firstname, lastname, username, password, created_date, status)
    VALUES ('test firstname', 'test lastname', 'shelter_owner@gmail.com', '$2a$10$yvSubCENPDb1zMsUMENBPOzFMbZRpS5tOMHlnNoMp2VkXCznQnjSq', '2023-03-05', 'ACTIVE');

INSERT INTO app_user_role
    VALUES (3, 3);

INSERT INTO app_user (firstname, lastname, username, password, created_date, status)
    VALUES ('test firstname', 'test lastname', 'volunteer@gmail.com', '$2a$10$G/vKpAS6IvWGzQ43kTLmHuK3G7Rbd8j4ts8/w/aKKTgsxRByUmr22', '2023-03-05', 'ACTIVE');

INSERT INTO app_user_role
    VALUES (4, 4);

