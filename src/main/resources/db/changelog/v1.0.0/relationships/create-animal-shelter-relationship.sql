ALTER TABLE "animal"
    ADD CONSTRAINT fk_animal_shelter FOREIGN KEY ("shelter_id") REFERENCES "shelter" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;