ALTER TABLE "lost_animal"
    ADD CONSTRAINT fk_lost_animal_animal FOREIGN KEY ("animal_id") REFERENCES "animal" ("id") ON DELETE CASCADE ON UPDATE CASCADE;