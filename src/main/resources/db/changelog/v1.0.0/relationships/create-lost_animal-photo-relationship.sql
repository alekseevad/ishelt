ALTER TABLE "photo"
    ADD CONSTRAINT fk_photo_lost_animal FOREIGN KEY ("lost_animal_id") REFERENCES "lost_animal" ("id") ON DELETE CASCADE ON UPDATE CASCADE;