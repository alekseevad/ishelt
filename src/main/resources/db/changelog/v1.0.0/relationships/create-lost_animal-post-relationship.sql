ALTER TABLE "post"
    ADD CONSTRAINT fk_post_lost_animal FOREIGN KEY ("lost_animal_id") REFERENCES "lost_animal" ("id") ON DELETE CASCADE ON UPDATE RESTRICT;