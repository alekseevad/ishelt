ALTER TABLE "post"
    ADD CONSTRAINT fk_post_app_user FOREIGN KEY ("app_user_id") REFERENCES "app_user" ("id") ON DELETE CASCADE ON UPDATE RESTRICT;