ALTER TABLE role_permission
    ADD CONSTRAINT fk_role_permission_user FOREIGN KEY (permission_id) REFERENCES permission (id) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE role_permission
    ADD CONSTRAINT fk_permission_role_user FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE ON UPDATE RESTRICT;