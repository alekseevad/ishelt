ALTER TABLE app_user_role
    ADD CONSTRAINT fk_user_roles_user FOREIGN KEY (app_user_id) REFERENCES app_user (id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE app_user_role
    ADD CONSTRAINT fk_user_roles_roles FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE ON UPDATE RESTRICT;