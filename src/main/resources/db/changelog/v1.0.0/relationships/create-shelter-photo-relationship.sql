ALTER TABLE "photo"
    ADD CONSTRAINT fk_photo_shelter FOREIGN KEY ("shelter_id") REFERENCES "shelter" ("id") ON DELETE CASCADE ON UPDATE CASCADE;