ALTER TABLE app_user_shelter
    ADD CONSTRAINT fk_user_shelter FOREIGN KEY (user_id) REFERENCES app_user (id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE app_user_shelter
    ADD CONSTRAINT fk_shelter_user FOREIGN KEY (shelter_id) REFERENCES shelter (id) ON DELETE CASCADE ON UPDATE RESTRICT;