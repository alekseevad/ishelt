CREATE TABLE "animal" (
    "id" SERIAL4 PRIMARY KEY NOT NULL,
    "shelter_id" SERIAL4 NOT NULL,
    "sex" VARCHAR NOT NULL,
    "breed" VARCHAR(50) NOT NULL,
    "coloring" VARCHAR(50) NOT NULL,
    "age" INTEGER NOT NULL,
    "size" FLOAT NOT NULL,
    "spayed_neutered" BOOLEAN NOT NULL,
    "intake_date" DATE NOT NULL,
    "description" text
)