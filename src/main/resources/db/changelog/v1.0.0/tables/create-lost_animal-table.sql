CREATE TABLE "lost_animal" (
    "id" SERIAL4 PRIMARY KEY NOT NULL,
    "animal_id" SERIAL4,
    "sex" VARCHAR(10) NOT NULL,
    "finding date" DATE NOT NULL,
    "coordinates" VARCHAR NOT NULL,
    "description" text
)