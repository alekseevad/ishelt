CREATE TABLE "photo" (
    "id" SERIAL4 PRIMARY KEY NOT NULL,
    "name" VARCHAR NOT NULL,
    "shelter_id" BIGINT,
    "post_id" BIGINT,
    "animal_id" BIGINT,
    "lost_animal_id" BIGINT
)