CREATE TABLE "post" (
    "id" SERIAL4 PRIMARY KEY NOT NULL,
    "app_user_id" SERIAL4,
    "animal_id" SERIAL4 NOT NULL,
    "lost_animal_id" SERIAL4 NOT NULL,
    "text" text NOT NULL,
    "creation_date" DATE NOT NULL
)