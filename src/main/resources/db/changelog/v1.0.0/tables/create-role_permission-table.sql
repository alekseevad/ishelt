CREATE TABLE "role_permission" (
   "id" SERIAL4 PRIMARY KEY,
   "role_id" SERIAL4 NOT NULL,
   "permission_id" SERIAL4 NOT NULL
)