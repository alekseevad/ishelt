CREATE TABLE "task" (
    "id" SERIAL4 PRIMARY KEY NOT NULL,
    "app_user_id" SERIAL4,
    "text" text NOT NULL,
    "creation_date" DATE NOT NULL,
    "status" boolean
)