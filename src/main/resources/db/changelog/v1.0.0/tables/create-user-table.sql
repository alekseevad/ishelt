create table app_user
(
    id serial4 primary key,
    firstname varchar(255),
    lastname varchar(255),
    username varchar(255) not null,
    password varchar(255) not null,
    created_date date not null,
    status varchar(255) not null
)