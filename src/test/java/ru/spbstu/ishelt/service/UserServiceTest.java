package ru.spbstu.ishelt.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.spbstu.ishelt.dto.CreateAppUserRequestDto;
import ru.spbstu.ishelt.exception.TechException;
import ru.spbstu.ishelt.model.AppUser;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserServiceTest {

    @Autowired
    private UserService userService;

    CreateAppUserRequestDto createAppUserRequestDto;
    String username;

//    @BeforeEach
//    void prepareData() {
//        username = "new_name";
//        createAppUserRequestDto = new CreateAppUserRequestDto("old_name",
//                username,
//                "firstname",
//                "lastname",
//                "password",
//                "USER");
//    }
//
//    @Test
//    void testCreateUserMethod() throws TechException {
//        userService.createUser(createAppUserRequestDto);
//        AppUser actual = userService.findByUsername(username).get();
//        assertEquals(username, actual.getUsername());
//    }

}